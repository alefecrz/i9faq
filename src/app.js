const express = require('express');

class AppController {
    //execulta a aplicação injetando as rotas e os middlewares
    constructor(){
        this.express = express();
        this.middlewares();
        this.routes();
    }

    middlewares(){
        this.express.use(express.json());
    }
    //importa as routas do arquio routes.js
    routes(){
        this.express.use(require('./routes'));
    }
}
//exporta um nova instancia do AppController
module.exports = new AppController().express;